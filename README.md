# OVITO: there is almost everything (maybe)

[![SonarCloud](https://sonarcloud.io/images/project_badges/sonarcloud-black.svg)](https://sonarcloud.io/summary/new_code?id=ima1647_OVITO)

## Application Link
http://ec2-18-216-132-143.us-east-2.compute.amazonaws.com/

## How to run locally

```bash
docker-compose up
```

**Warning:** for the development, serve locally (front-end)

## Features

0. Base of the app, product creation form, profile page [Alina]
1. Cards for products, product editing [Natasha]
2. Searching elements insertion, categories form [Ruslan]
3. Authorization and personal accounts [Nikita]
4. Integration with Yandex Maps _+ location detection_ [Marina]

[//]: # (6. Automatized sending of the advertisements to different telegram chats according to the item category [Niki])

## Todo (global), required for the mark

- [x] Add backend (api)
- [x] Add CI
  - [x] Static analysis
  - [x] Unit testing (Coverage from 60% statement)
  - [x] Mutation testing
  - [x] Stress testing
  - [x] UI testing
  - [x] Dashboard and service monitoring
- [x] Add CD

## Dashboards
Loki - http://ec2-18-216-132-143.us-east-2.compute.amazonaws.com:3000/d/MQHVDmtWk/loki2-0-global-metrics?orgId=1

Prometheus - http://ec2-18-216-132-143.us-east-2.compute.amazonaws.com:3000/d/KnK-Qy_7k/prometheus-2-0-overview?orgId=1&refresh=30s

## References

- [Design](https://www.figma.com/file/BTfablSBji62eVrUn56b8k/Ovito?node-id=0%3A1)
