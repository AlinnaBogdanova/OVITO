CREATE_CATEGORY_QUERY = """
    INSERT INTO category
    VALUES (:id, :name)
    RETURNING id, name
"""

GET_CATEGORY_BY_ID_QUERY = """
    SELECT * FROM category
    WHERE id = :category_id
"""

GET_CATEGORY_BY_NAME_QUERY = """
    SELECT * FROM category
    WHERE name = :category_name
"""

GET_ALL_CATEGORIES_QUERY = """
    SELECT * FROM category
"""

UPDATE_CATEGORY_QUERY = """
    UPDATE category
    SET name = :name
    WHERE id = :id
    RETURNING *
"""

DELETE_CATEGORY_QUERY = """
    DELETE FROM category
    WHERE id = :category_id
"""
