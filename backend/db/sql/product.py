CREATE_PRODUCT_QUERY = """
    INSERT INTO product
    VALUES (:id, :title, :description, :location, :time, :category_id, :price, :user_id, :status)
    RETURNING id, title, description, location, time, category_id, price, user_id, status
"""

GET_ALL_PRODUCTS_QUERY = """
    SELECT * FROM product
"""

GET_USER_PRODUCT_QUERY = """
    SELECT * FROM product
    WHERE id = :product_id AND user_id = :user_id
"""

ADD_IMAGE_TO_PRODUCT_QUERY = """
    INSERT INTO product_image
    VALUES (:product_id, :image_id)
"""

GET_ALL_PRODUCT_IMAGES_QUERY = """
    SELECT * FROM product_image
    WHERE product_id = :product_id
"""

GET_USERS_PRODUCTS_QUERY = """
    SELECT * FROM product
    WHERE user_id = :user_id
"""

UPDATE_PRODUCT_QUERY = """
    UPDATE product
    SET title = :title,
        description = :description,
        location  = :location,
        category_id = :category_id,
        price = :price
    WHERE product.id = :id
    RETURNING *
"""

DELETE_PRODUCT_QUERY = """
    DELETE FROM product
    WHERE id = :product_id
"""

GET_PRODUCT_QUERY = """
    SELECT * FROM product
    WHERE id = :product_id
"""

GET_PRODUCTS_BY_CATEGORY_ID_QUERY = """
    SELECT * FROM product
    WHERE category_id = :category_id
"""