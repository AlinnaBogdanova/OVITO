CREATE_IMAGE_RECORD_QUERY = """
    INSERT INTO image
    VALUES (:id, :filename)
    RETURNING id, filename
"""

GET_IMAGE_BY_ID_QUERY = """
    SELECT * FROM image
    WHERE id = :id
"""