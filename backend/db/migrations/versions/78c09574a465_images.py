
"""images

Revision ID: 78c09574a465
Revises: 86655b6ed730
Create Date: 2022-03-13 23:07:00.085288

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy_utils import UUIDType


# revision identifiers, used by Alembic
revision = '78c09574a465'
down_revision = '86655b6ed730'
branch_labels = None
depends_on = None


def create_image_table():
    op.create_table(
        "image",
        sa.Column("id", UUIDType(binary=False), index=True, primary_key=True),
        sa.Column("filename", sa.String(length=320), nullable=False)
    )


def upgrade() -> None:
    create_image_table()


def downgrade() -> None:
    op.drop_table("image")
