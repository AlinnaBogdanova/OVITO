
"""product-image

Revision ID: 7e074dd2042a
Revises: b0d37bae45ea
Create Date: 2022-03-14 01:30:23.701127

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy_utils import UUIDType


# revision identifiers, used by Alembic
revision = '7e074dd2042a'
down_revision = 'b0d37bae45ea'
branch_labels = None
depends_on = None


def create_product_image_table():
    op.create_table(
        "product_image",
        sa.Column("product_id", UUIDType(binary=False),
                  sa.ForeignKey("product.id", ondelete="CASCADE", onupdate="CASCADE"), index=True, nullable=False),
        sa.Column("image_id", UUIDType(binary=False),
                  sa.ForeignKey("image.id", ondelete="CASCADE", onupdate="CASCADE"), nullable=False)
    )


def upgrade() -> None:
    create_product_image_table()


def downgrade() -> None:
    op.drop_table("product_image")
