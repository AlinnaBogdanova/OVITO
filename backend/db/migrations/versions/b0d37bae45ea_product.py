
"""product

Revision ID: b0d37bae45ea
Revises: 78c09574a465
Create Date: 2022-03-14 00:41:11.315544

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy_utils import UUIDType


# revision identifiers, used by Alembic
revision = 'b0d37bae45ea'
down_revision = '78c09574a465'
branch_labels = None
depends_on = None


def create_product_table():
    op.create_table(
        "product",
        sa.Column("id", UUIDType(binary=False), primary_key=True),
        sa.Column("title", sa.String(length=320), index=True, nullable=False),
        sa.Column("description", sa.String(length=2500)),
        sa.Column("location", sa.String(length=320)),
        sa.Column("time", sa.DateTime),
        sa.Column("category_id", UUIDType(binary=False), sa.ForeignKey("category.id", ondelete="CASCADE", onupdate="CASCADE")),
        sa.Column("price", sa.Integer),
        sa.Column("user_id", UUIDType(binary=False), sa.ForeignKey("user.id", ondelete="CASCADE", onupdate="CASCADE")),
        sa.Column("status", sa.Integer)
    )


def upgrade() -> None:
    create_product_table()


def downgrade() -> None:
    op.drop_table("product")
