from databases import Database
from backend.core.config import DATABASE_URL

database = Database(DATABASE_URL)
