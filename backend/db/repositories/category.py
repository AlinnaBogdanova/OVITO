from backend.db.repositories.base import BaseRepository
from backend.models.category import Category, CategoryUpdate
from backend.db.sql.category import (CREATE_CATEGORY_QUERY, GET_CATEGORY_BY_ID_QUERY,
                                     GET_CATEGORY_BY_NAME_QUERY, GET_ALL_CATEGORIES_QUERY, UPDATE_CATEGORY_QUERY,
                                     DELETE_CATEGORY_QUERY)
from fastapi import HTTPException
from starlette.status import HTTP_400_BAD_REQUEST, HTTP_404_NOT_FOUND
from pydantic import UUID4


class CategoryRepository(BaseRepository):
    """Repository of the Categories"""
    async def add_category(self, category: Category):
        """Add new Category"""
        try:
            created_category = await self.db.fetch_one(query=CREATE_CATEGORY_QUERY, values=category.dict())
            return Category(**created_category)
        except Exception as e:
            raise HTTPException(status_code=HTTP_400_BAD_REQUEST, detail="Category with the same name already exists")

    async def get_category_by_id(self, category_id: UUID4):
        """Get Category by its ID"""
        try:
            category = await self.db.fetch_one(query=GET_CATEGORY_BY_ID_QUERY, values={"category_id": category_id})
        except Exception as e:
            raise HTTPException(status_code=HTTP_400_BAD_REQUEST, detail="Couldn't fetch category, try again")

        if not category:
            raise HTTPException(status_code=HTTP_404_NOT_FOUND, detail="There is no category with such ID")
        return Category(**category)

    async def get_category_by_name(self, category_name: str):
        """Get Category by its Name"""
        try:
            category = await self.db.fetch_one(query=GET_CATEGORY_BY_NAME_QUERY, values={"category_name": category_name})
        except Exception as e:
            raise HTTPException(status_code=HTTP_400_BAD_REQUEST, detail="Couldn't fetch category, try again")

        if not category:
            raise HTTPException(status_code=HTTP_404_NOT_FOUND, detail="There is no category with such name")
        return Category(**category)

    async def get_all_categories(self):
        """Get all categories"""
        try:
            all_categories = await self.db.fetch_all(query=GET_ALL_CATEGORIES_QUERY)
        except Exception as e:
            raise HTTPException(status_code=HTTP_400_BAD_REQUEST, detail="Something went wrong, try again")

        if not all_categories:
            return []

        def cast_to_category(category):
            return Category(**category)

        return list(map(cast_to_category, all_categories))

    async def patch_category(self, category_id: UUID4, update_category: CategoryUpdate):
        category_db = await self.get_category_by_id(category_id)

        updated_category = category_db.copy(update=update_category.dict(exclude_unset=True))
        try:
            updated_category_db = await self.db.fetch_one(query=UPDATE_CATEGORY_QUERY, values=updated_category.dict())
            return Category(**updated_category_db)
        except Exception as e:
            raise HTTPException(status_code=HTTP_400_BAD_REQUEST, detail="Something went wrong, try again")

    async def delete_category(self, category_id: UUID4):
        try:
            await self.db.fetch_one(query=DELETE_CATEGORY_QUERY, values={"category_id": category_id})
        except Exception as e:
            raise HTTPException(status_code=HTTP_400_BAD_REQUEST, detail="Something went wrong, try again")
