from databases import Database


class BaseRepository:
    """Base Repository"""
    def __init__(self, db: Database) -> None:
        self.db = db
