from backend.db.repositories.base import BaseRepository
from backend.models.image import ImageDB
from backend.db.sql.image import CREATE_IMAGE_RECORD_QUERY, GET_IMAGE_BY_ID_QUERY
from fastapi import HTTPException
from starlette.status import HTTP_400_BAD_REQUEST, HTTP_404_NOT_FOUND
from pydantic import UUID4


class ImageRepository(BaseRepository):
    """Image Repository"""
    async def create_image(self, image: ImageDB):
        """Create Image record in DB"""
        try:
            created_image = await self.db.fetch_one(query=CREATE_IMAGE_RECORD_QUERY, values=image.dict())
            return ImageDB(**created_image)
        except Exception as e:
            raise HTTPException(status_code=HTTP_400_BAD_REQUEST, detail="Something went wrong, try again")

    async def get_image_by_id(self, image_id: UUID4):
        """Get Image record by ID"""
        try:
            image = await self.db.fetch_one(query=GET_IMAGE_BY_ID_QUERY, values={"id": image_id})
            return ImageDB(**image)
        except Exception as e:
            raise HTTPException(status_code=HTTP_404_NOT_FOUND, detail="Couldn't find Image, try again")