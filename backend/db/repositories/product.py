from backend.db.repositories.base import BaseRepository
from backend.models.product import ProductDB, ProductUpdate
from backend.db.sql.product import (CREATE_PRODUCT_QUERY, GET_ALL_PRODUCTS_QUERY, GET_USER_PRODUCT_QUERY,
                                    ADD_IMAGE_TO_PRODUCT_QUERY, GET_ALL_PRODUCT_IMAGES_QUERY, GET_USERS_PRODUCTS_QUERY,
                                    UPDATE_PRODUCT_QUERY, DELETE_PRODUCT_QUERY, GET_PRODUCT_QUERY,
                                    GET_PRODUCTS_BY_CATEGORY_ID_QUERY)
from fastapi import HTTPException
from starlette.status import HTTP_400_BAD_REQUEST, HTTP_403_FORBIDDEN, HTTP_404_NOT_FOUND
from backend.models.product_image import ProductImage
from backend.models.user import User
from pydantic import UUID4


class ProductRepository(BaseRepository):
    """Product Repository"""
    async def create_product(self, product: ProductDB):
        try:
            created_product = await self.db.fetch_one(query=CREATE_PRODUCT_QUERY, values=product.dict())
            return ProductDB(**created_product)
        except Exception as e:
            raise HTTPException(status_code=HTTP_400_BAD_REQUEST, detail="Couldn't create Product, try again")

    async def get_all_products(self):
        try:
            all_products = await self.db.fetch_all(query=GET_ALL_PRODUCTS_QUERY)

            if not all_products:
                return []

            def cast_to_product(product):
                return ProductDB(**product)

            return list(map(cast_to_product, all_products))
        except Exception as e:
            raise HTTPException(status_code=HTTP_400_BAD_REQUEST, detail="Something went wrong, try again")

    async def get_users_products(self, user_id: UUID4):
        try:
            users_products = await self.db.fetch_all(query=GET_USERS_PRODUCTS_QUERY, values={"user_id": user_id})

            if not users_products:
                return []

            def cast_to_product(product):
                return ProductDB(**product)

            return list(map(cast_to_product, users_products))
        except Exception as e:
            raise HTTPException(status_code=HTTP_400_BAD_REQUEST, detail="Something went wrong, try again")

    async def add_image(self, product_image: ProductImage, user: User):
        try:
            product_check_user = await self.db.fetch_one(query=GET_USER_PRODUCT_QUERY,
                                                   values={"product_id": product_image.product_id, "user_id": user.id})
        except Exception as e:
            raise HTTPException(status_code=HTTP_400_BAD_REQUEST, detail="Something went wrong, try again")

        print(product_check_user)
        if not product_check_user:
            raise HTTPException(status_code=HTTP_403_FORBIDDEN, detail="Can't add images to another's product")

        try:
            await self.db.fetch_one(query=ADD_IMAGE_TO_PRODUCT_QUERY, values=product_image.dict())
        except Exception as e:
            raise HTTPException(status_code=HTTP_400_BAD_REQUEST, detail="Something went wrong, try again")

    async def get_all_product_images(self, product_id: UUID4):
        try:
            all_images = await self.db.fetch_all(query=GET_ALL_PRODUCT_IMAGES_QUERY, values={"product_id": product_id})
        except Exception as e:
            raise HTTPException(status_code=HTTP_400_BAD_REQUEST, detail="Something went wrong, try again")

        if not all_images:
            return []

        def get_images_ids(product_image):
            return product_image["image_id"]

        return list(map(get_images_ids, all_images))

    async def patch_product(self, product_id: UUID4, user_id: UUID4, updated_product: ProductUpdate):
        product_db = await self.db.fetch_one(query=GET_USER_PRODUCT_QUERY,
                                             values={"product_id": product_id, "user_id": user_id})
        if not product_db:
            raise HTTPException(status_code=HTTP_404_NOT_FOUND, detail="User doesn't have such product")

        product_db = ProductDB(**product_db)
        new_updated_product = product_db.copy(update=updated_product.dict(exclude_unset=True))
        values = {
            "title": new_updated_product.title,
            "description": new_updated_product.description,
            "location": new_updated_product.location,
            "category_id": new_updated_product.category_id,
            "price": new_updated_product.price,
            "id": new_updated_product.id
        }
        try:
            updated_product_db = await self.db.fetch_one(query=UPDATE_PRODUCT_QUERY, values=values)
            return ProductDB(**updated_product_db)
        except Exception as e:
            raise HTTPException(status_code=HTTP_400_BAD_REQUEST, detail="Something went wrong, try again")

    async def delete_product(self, product_id: UUID4, user_id: UUID4):
        product_db = await self.db.fetch_one(query=GET_USER_PRODUCT_QUERY,
                                             values={"product_id": product_id, "user_id": user_id})

        if not product_db:
            raise HTTPException(status_code=HTTP_404_NOT_FOUND, detail="User doesn't have such product")

        try:
            await self.db.fetch_one(query=DELETE_PRODUCT_QUERY, values={"product_id": product_id})
        except Exception as e:
            raise HTTPException(status_code=HTTP_400_BAD_REQUEST, detail="Something went wrong, try again")

    async def get_product(self, product_id: UUID4):
        product_db = await self.db.fetch_one(query=GET_PRODUCT_QUERY, values={"product_id": product_id})

        if not product_db:
            raise HTTPException(status_code=HTTP_404_NOT_FOUND, detail="No such product")

        return ProductDB(**product_db)

    async def get_products_by_category_id(self, category_id: UUID4):
        try:
            products = await self.db.fetch_all(query=GET_PRODUCTS_BY_CATEGORY_ID_QUERY,
                                               values={"category_id": category_id})

            if not products:
                return []

            def cast_to_product(product):
                return ProductDB(**product)

            return list(map(cast_to_product, products))
        except Exception as e:
            raise HTTPException(status_code=HTTP_400_BAD_REQUEST, detail="Something went wrong, try again")
