import pytest
from httpx import AsyncClient
from fastapi import FastAPI
from starlette.status import HTTP_201_CREATED, HTTP_204_NO_CONTENT, HTTP_200_OK, HTTP_202_ACCEPTED
from typing import Dict
import json


@pytest.mark.asyncio
class TestProductsRoutes:
    async def test_01_get_all_products(self, app: FastAPI, authorized_client: AsyncClient) -> None:
        res = await authorized_client.get(app.url_path_for("products:get_all_products"))
        assert res.status_code == HTTP_200_OK
        assert res.json() == []

    async def test_02_create_product(self, app: FastAPI, authorized_client: AsyncClient,
                                     product: Dict, category: Dict) -> None:
        new_product = product.copy()
        new_product["category_id"] = category["id"]
        res = await authorized_client.post(app.url_path_for("products:create_product"), data=json.dumps(new_product))
        assert res.status_code == HTTP_201_CREATED
        assert all(new_product[key] == res.json()[key] for key in new_product.keys())
        assert res.json()["category_id"] == category["id"]

    async def test_03_get_product_by_id(self, app: FastAPI, authorized_client: AsyncClient,
                                        product: Dict, category: Dict) -> None:
        res = await authorized_client.get(app.url_path_for("products:get_all_products"))
        products = res.json()
        assert len(products) == 1
        db_product = products[0]
        res = await authorized_client.get(f"/api/v1/product/{db_product['id']}")
        assert res.status_code == HTTP_200_OK
        assert all(product[key] == res.json()[key] for key in product.keys())
        assert res.json()["category_id"] == category["id"]

    async def test_04_get_products_of_user(self, app: FastAPI, authorized_client: AsyncClient,
                                           product: Dict, category: Dict) -> None:
        res = await authorized_client.get("/api/v1/account/me")
        user_id = res.json()["id"]
        res = await authorized_client.get(app.url_path_for("products:get_products_of_user"),
                                          params={"user_id": user_id})
        assert res.status_code == HTTP_200_OK
        assert len(res.json()) == 1
        assert all(product[key] == res.json()[0][key] for key in product.keys())
        assert res.json()[0]["category_id"] == category["id"]

    async def test_05_get_products_of_category(self, app: FastAPI, authorized_client: AsyncClient,
                                               product: Dict, category: Dict):
        res = await authorized_client.get(f"/api/v1/product/by_category/{category['id']}")
        assert res.status_code == HTTP_200_OK
        assert len(res.json()) == 1
        assert all(product[key] == res.json()[0][key] for key in product.keys())
        assert res.json()[0]["category_id"] == category["id"]

    async def test_06_patch_product(self, app: FastAPI, authorized_client: AsyncClient,
                                    patch_product: Dict, category: Dict):
        res = await authorized_client.get(app.url_path_for("products:get_all_products"))
        products = res.json()
        assert len(products) == 1
        db_product = products[0]
        res = await authorized_client.patch(f"/api/v1/product/{db_product['id']}",
                                            data=json.dumps(patch_product))
        assert res.status_code == HTTP_202_ACCEPTED
        assert all(patch_product[key] == res.json()[key] for key in patch_product.keys())
        assert res.json()["category_id"] == category["id"]

    async def test_07_delete_product(self, app: FastAPI, authorized_client: AsyncClient, product: Dict) -> None:
        res = await authorized_client.get(app.url_path_for("products:get_all_products"))
        products = res.json()
        assert len(products) == 1
        db_product = products[0]
        res = await authorized_client.delete(f"/api/v1/product/{db_product['id']}")
        assert res.status_code == HTTP_204_NO_CONTENT
        res = await authorized_client.get(app.url_path_for("products:get_all_products"))
        assert res.status_code == HTTP_200_OK
        assert res.json() == []
