import pytest
from httpx import AsyncClient
from fastapi import FastAPI
from starlette.status import HTTP_201_CREATED, HTTP_204_NO_CONTENT, HTTP_200_OK, HTTP_202_ACCEPTED
from typing import Dict
import json


@pytest.mark.asyncio
class TestCategoriesRoutes:
    async def test_01_get_all_categories(self, app: FastAPI, authorized_client: AsyncClient, category: Dict) -> None:
        res = await authorized_client.get(app.url_path_for("categories:get_all_categories"))
        assert res.status_code == HTTP_200_OK
        assert len(res.json()) == 1
        assert all(category[key] == res.json()[0][key] for key in category.keys())

    async def test_02_create_category(self, app: FastAPI, authorized_client: AsyncClient, category2: Dict) -> None:
        res = await authorized_client.post(app.url_path_for("categories:create_category"), data=json.dumps(category2))
        assert res.status_code == HTTP_201_CREATED
        assert res.json()["name"] == category2["name"]

    async def test_03_get_category_by_id(self, app: FastAPI, authorized_client: AsyncClient, category: Dict):
        res = await authorized_client.get(f"/api/v1/category/by_id/{category['id']}")
        assert res.status_code == HTTP_200_OK
        assert all(res.json()[key] == category[key] for key in category.keys())

    async def test_04_get_category_by_name(self, app: FastAPI, authorized_client: AsyncClient, category: Dict):
        res = await authorized_client.get(f"/api/v1/category/by_name/{category['name']}")
        assert res.status_code == HTTP_200_OK
        assert all(res.json()[key] == category[key] for key in category.keys())

    async def test_05_patch_category(self, app: FastAPI, authorized_client: AsyncClient,
                                     patch_category: Dict, category2: Dict):
        res = await authorized_client.get(f"/api/v1/category/by_name/{category2['name']}")
        assert res.status_code == HTTP_200_OK
        cat_id = res.json()["id"]
        res = await authorized_client.patch(f"/api/v1/category/{cat_id}", data=json.dumps(patch_category))
        assert res.status_code == HTTP_202_ACCEPTED
        assert res.json()["id"] == cat_id
        assert res.json()["name"] == patch_category["name"]

    async def test_06_delete_category(self, app: FastAPI, authorized_client: AsyncClient, patch_category: Dict):
        res = await authorized_client.get(f"/api/v1/category/by_name/{patch_category['name']}")
        assert res.status_code == HTTP_200_OK
        res = await authorized_client.delete(f"/api/v1/category/{res.json()['id']}")
        assert res.status_code == HTTP_204_NO_CONTENT
