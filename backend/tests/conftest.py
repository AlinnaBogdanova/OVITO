import warnings
import logging
import os
import pytest
from asgi_lifespan import LifespanManager
from fastapi import FastAPI
from httpx import AsyncClient
from databases import Database
import alembic
from alembic.config import Config
from databases import Database
from typing import Dict
import json
from backend.models.category import Category
import asyncio


@pytest.fixture(scope="session")
def event_loop():
    return asyncio.get_event_loop()


# Create a new application for testing
@pytest.fixture(scope="session")
def app() -> FastAPI:
    from backend.api.app import get_application
    return get_application()


@pytest.fixture(scope="session")
def db(app: FastAPI) -> Database:
    return app.state.db


@pytest.fixture(scope="session")
async def client(app: FastAPI) -> AsyncClient:
    async with LifespanManager(app):
        async with AsyncClient(
            app=app,
            base_url="http://backend",
            headers={"Content-Type": "application/json"}
        ) as client:
            yield client


@pytest.fixture(scope="session")
async def account(db: Database, client: AsyncClient):
    register_path = "/api/v1/account/register"
    user_data = {"email": "user@example.com",
                  "password": "string",
                  "is_active": 'true',
                  "is_superuser": 'false',
                  "is_verified": 'false',
                  "name": "string",
                  "telegram_alias": "string",
                  "phone": "string",
                  "address": "string"}
    res = await client.post(register_path, data=json.dumps(user_data))
    user = res.json()
    user["password"] = "string"
    yield user
    delete_user_query = """DELETE FROM "user"
                           WHERE id = :id"""
    await db.fetch_one(query=delete_user_query, values={"id": user["id"]})


@pytest.fixture(scope="session")
async def authorized_client(client: AsyncClient, account: Dict):
    login_path = "/api/v1/account/login"
    client.headers.update({"Content-Type": "application/x-www-form-urlencoded"})
    login_data = {"grand_type": "",
                  "username": account["email"],
                  "password": account["password"],
                  "scope": "",
                  "client_id": "",
                  "client_secret": ""}
    res = await client.post(login_path, data=login_data)
    client.headers.update({"Content-Type": "application/json"})
    yield client


@pytest.fixture(scope="session")
async def category(db: Database):
    new_category = Category(name="category1")
    create_cat_query = """INSERT INTO category
                          VALUES (:id, :name)
                          RETURNING id, name"""
    category = await db.fetch_one(query=create_cat_query, values=new_category.dict())
    category = {"id": str(category["id"]), "name": category["name"]}
    yield category
    delete_category_query = """DELETE FROM category
                               WHERE id = :id"""
    await db.fetch_one(query=delete_category_query, values={"id": category["id"]})


@pytest.fixture
async def product():
    product = {
            "title": "string",
            "description": "string",
            "location": "string",
            "price": 10000000
            }
    return product


@pytest.fixture
async def patch_product():
    product = {
        "title": "string1",
        "description": "string1",
        "location": "string1",
        "price": 9999
    }
    return product


@pytest.fixture
async def category2():
    return {"name": "category2"}


@pytest.fixture
async def patch_category():
    return {"name": "category_patched"}
