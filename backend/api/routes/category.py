from fastapi import APIRouter, Body, Depends
from backend.models.category import Category, CategoryCreate, CategoryUpdate
from backend.models.user import User
from starlette.status import HTTP_201_CREATED, HTTP_200_OK, HTTP_202_ACCEPTED, HTTP_204_NO_CONTENT
from backend.api.routes.account import fastapi_users
from backend.db.repositories.category import CategoryRepository
from backend.api.dependencies.database import get_repository
from pydantic import UUID4, Field
from typing import List

router = APIRouter(prefix='/category', tags=["categories"])

current_user = fastapi_users.current_user(active=True)


@router.post("/", response_model=Category, status_code=HTTP_201_CREATED, name="categories:create_category")
async def create_category(
        user: User = Depends(current_user),
        new_category: CategoryCreate = Body(...),
        category_repo: CategoryRepository = Depends(get_repository(CategoryRepository))
        ) -> Category:
    """Create new Category"""
    category: Category = Category(**new_category.dict())
    created_category: Category = await category_repo.add_category(category)
    return created_category


@router.get("/by_id/{category_id}", response_model=Category, status_code=HTTP_200_OK)
async def get_category_by_id(
        category_id: UUID4,
        user: User = Depends(current_user),
        category_repo: CategoryRepository = Depends(get_repository(CategoryRepository))
        ) -> Category:
    """Get Category be its ID"""
    category: Category = await category_repo.get_category_by_id(category_id)
    return category


@router.get("/by_name/{category_name}", response_model=Category, status_code=HTTP_200_OK)
async def get_category_by_name(
        category_name: str,
        user: User = Depends(current_user),
        category_repo: CategoryRepository = Depends(get_repository(CategoryRepository))
        ) -> Category:
    """Get Category by its Name"""
    category: Category = await category_repo.get_category_by_name(category_name)
    return category


@router.get("/", response_model=List[Category], status_code=HTTP_200_OK, name="categories:get_all_categories")
async def get_all_categories(
        user: User = Depends(current_user),
        category_repo: CategoryRepository = Depends(get_repository(CategoryRepository))
        ) -> List[Category]:
    """Get list of all Categories"""
    all_categories = await category_repo.get_all_categories()
    return all_categories


@router.patch("/{category_id}", response_model=Category, status_code=HTTP_202_ACCEPTED)
async def patch_category(
        category_id: UUID4,
        update_category: CategoryUpdate,
        user: User = Depends(current_user),
        category_repo: CategoryRepository = Depends(get_repository(CategoryRepository))
        ) -> Category:
    """Patch Category by its ID"""
    category = await category_repo.patch_category(category_id=category_id, update_category=update_category)
    return category


@router.delete("/{category_id}", status_code=HTTP_204_NO_CONTENT)
async def delete_category(
        category_id: UUID4,
        user: User = Depends(current_user),
        category_repo: CategoryRepository = Depends(get_repository(CategoryRepository))
        ) -> None:
    """Delete Category by its ID"""
    await category_repo.delete_category(category_id=category_id)
