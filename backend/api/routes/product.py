from fastapi import APIRouter, Depends, Body
from backend.models.product import ProductDB, ProductCreate, ProductUpdate
from starlette.status import HTTP_201_CREATED, HTTP_200_OK, HTTP_202_ACCEPTED, HTTP_204_NO_CONTENT
from backend.api.routes.account import fastapi_users
from backend.models.user import User
from backend.api.dependencies.database import get_repository
from backend.db.repositories.product import ProductRepository
from typing import List
from backend.models.product_image import ProductImage
from pydantic import UUID4

router = APIRouter(prefix="/product", tags=["products"])

current_user = fastapi_users.current_user(active=True)


@router.post("/", response_model=ProductDB, status_code=HTTP_201_CREATED, name="products:create_product")
async def create_product(
        product: ProductCreate = Body(...),
        user: User = Depends(current_user),
        product_repo: ProductRepository = Depends(get_repository(ProductRepository))
        ) -> ProductDB:
    """Create new Product"""
    product = ProductDB(**product.dict(), user_id=user.id)
    created_product = await product_repo.create_product(product)
    return created_product


@router.get("/", response_model=List[ProductDB], status_code=HTTP_200_OK, name="products:get_all_products")
async def get_all_products(
        user: User = Depends(current_user),
        product_repo: ProductRepository = Depends(get_repository(ProductRepository))
        ) -> List[ProductDB]:
    """Get list of all Products"""
    all_products = await product_repo.get_all_products()
    return all_products


@router.get("/of", response_model=List[ProductDB], status_code=HTTP_200_OK, name="products:get_products_of_user")
async def get_products_of_user(
        user_id: UUID4,
        user: User = Depends(current_user),
        product_repo: ProductRepository = Depends(get_repository(ProductRepository))
        ) -> List[ProductDB]:
    """Get list of all user's products"""
    users_products = await product_repo.get_users_products(user_id=user_id)
    return users_products


@router.post("/add_image", status_code=HTTP_201_CREATED)
async def add_image_to_product(
        product_image: ProductImage = Body(...),
        user: User = Depends(current_user),
        product_repo: ProductRepository = Depends(get_repository(ProductRepository))
        ) -> None:
    """Add Image to the Product"""
    await product_repo.add_image(product_image, user)


@router.get("/{product_id}/images", response_model=List[UUID4], status_code=HTTP_200_OK)
async def get_images_of_the_product(
        product_id: UUID4,
        product_repo: ProductRepository = Depends(get_repository(ProductRepository))
        ) -> List[UUID4]:
    """Returns list of all images of the product"""
    image_list = await product_repo.get_all_product_images(product_id)
    return image_list


@router.patch("/{product_id}", response_model=ProductDB, status_code=HTTP_202_ACCEPTED, name="products:patch_product")
async def patch_product(
        product_id: UUID4,
        updated_product: ProductUpdate,
        user: User = Depends(current_user),
        product_repo: ProductRepository = Depends(get_repository(ProductRepository))
        ) -> ProductDB:
    """Patch existing product by its ID"""
    product = await product_repo.patch_product(product_id=product_id, user_id=user.id, updated_product=updated_product)
    return product


@router.delete("/{product_id}", status_code=HTTP_204_NO_CONTENT, name="products:delete_product")
async def delete_product(
        product_id: UUID4,
        user: User = Depends(current_user),
        product_repo: ProductRepository = Depends(get_repository(ProductRepository))
        ) -> None:
    """Delete product by its ID"""
    await product_repo.delete_product(product_id=product_id, user_id=user.id)


@router.get("/{product_id}", response_model=ProductDB, status_code=HTTP_200_OK, name="products:get_product")
async def get_product_by_id(
        product_id: UUID4,
        user: User = Depends(current_user),
        product_repo: ProductRepository = Depends(get_repository(ProductRepository))
        ) -> ProductDB:
    """Get product by its ID"""
    product = await product_repo.get_product(product_id=product_id)
    return product


@router.get("/by_category/{category_id}", response_model=List[ProductDB], name="products:get_products_of_category")
async def get_products_by_category_id(
        category_id: UUID4,
        user: User = Depends(current_user),
        product_repo: ProductRepository = Depends(get_repository(ProductRepository))
        ) -> List[ProductDB]:
    products = await product_repo.get_products_by_category_id(category_id=category_id)
    return products
