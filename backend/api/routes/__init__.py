from fastapi import APIRouter
from backend.api.routes.account import router as account_router
from backend.api.routes.category import router as category_router
from backend.api.routes.image import router as image_router
from backend.api.routes.product import router as product_router

router = APIRouter(prefix="/v1", tags=["v1"])  # router /api/v1

router.include_router(account_router)  # Account routes
router.include_router(category_router)  # Category routes
router.include_router(image_router)  # Image routes
router.include_router(product_router)  # Product routes
