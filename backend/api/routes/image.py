from fastapi import APIRouter, File, UploadFile, Depends, HTTPException
from starlette.status import HTTP_201_CREATED, HTTP_200_OK, HTTP_404_NOT_FOUND, HTTP_400_BAD_REQUEST
from backend.api.routes.account import fastapi_users
from backend.models.image import Image, ImageDB
from fastapi.responses import FileResponse
from backend.api.dependencies.database import get_repository
from backend.db.repositories.image import ImageRepository
from backend.models.user import User
import aiofiles
import os
from pydantic import UUID4

router = APIRouter(prefix="/image", tags=["images"])

current_user = fastapi_users.current_user(active=True)


@router.post("/", response_model=ImageDB, status_code=HTTP_201_CREATED)
async def upload_image(
        image: UploadFile = File(...),
        user: User = Depends(current_user),
        image_repo: ImageRepository = Depends(get_repository(ImageRepository))
        ) -> ImageDB:
    """Upload Image"""

    async def save_image_file(filename: str, file_data: bytes):
        async with aiofiles.open(f"/images/{filename}", "wb") as image_file:
            await image_file.write(file_data)
            await image_file.close()

    if image.content_type.split('/')[0] != "image":
        raise HTTPException(status_code=HTTP_400_BAD_REQUEST, detail="File must be image")

    if not os.path.isdir("/images"):
        os.mkdir("/images")

    image_file_data = await image.read()

    image_record = ImageDB(filename=image.filename)

    created_image = await image_repo.create_image(image_record)
    await save_image_file(str(created_image.id), image_file_data)
    return created_image


@router.get("/{image_id}", status_code=HTTP_200_OK)
async def get_image_by_id(
        image_id: UUID4,
        user: User = Depends(current_user),
        image_repo: ImageRepository = Depends(get_repository(ImageRepository))
        ) -> FileResponse:
    """Get Image by its ID"""
    image_record = await image_repo.get_image_by_id(image_id)

    if not os.path.exists(f"/images/{str(image_record.id)}"):
        raise HTTPException(status_code=HTTP_404_NOT_FOUND, detail="Image doesn't exist")

    return FileResponse(f"/images/{str(image_record.id)}", filename=image_record.filename)
