from fastapi import FastAPI
from backend.api.routes import router as v1_router
from backend.core.tasks import create_start_app_handler, create_stop_app_handler
from backend.db import database
from fastapi.middleware.cors import CORSMiddleware
from starlette_exporter import PrometheusMiddleware, handle_metrics


def get_application():
    """Returns FastAPI application with all the settings"""
    app = FastAPI()
    app.include_router(v1_router, prefix="/api")

    app.add_event_handler("startup", create_start_app_handler(app, database))
    app.add_event_handler("shutdown", create_stop_app_handler(app))

    app.add_middleware(
        CORSMiddleware,
        allow_origins=["http://localhost:8080", "localhost:8080", "http://localhost:80", "localhost:80", "localhost", "http://ec2-18-216-132-143.us-east-2.compute.amazonaws.com:80", "ec2-18-216-132-143.us-east-2.compute.amazonaws.com:80", "ec2-18-216-132-143.us-east-2.compute.amazonaws.com"],
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )
    app.add_middleware(PrometheusMiddleware)
    app.add_route("/metrics", handle_metrics)

    return app


app = get_application()  # Application
