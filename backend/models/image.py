from pydantic import BaseModel, UUID4, Field
from uuid import uuid4


class Image(BaseModel):
    filename: str = Field(...)


class ImageDB(Image):
    id: UUID4 = Field(default_factory=uuid4)
