from pydantic import BaseModel, UUID4, Field
from uuid import uuid4


class Category(BaseModel):
    """Category Model"""
    id: UUID4 = Field(default_factory=uuid4)
    name: str = Field(..., min_length=3, max_length=320)


class CategoryCreate(BaseModel):
    """Create Category Model"""
    name: str = Field(..., min_length=3, max_length=320)


class CategoryUpdate(BaseModel):
    """Update Category Model"""
    name: str = Field(min_length=3, max_length=320)
