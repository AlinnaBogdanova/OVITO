from fastapi_users import models
from pydantic import BaseModel, Field
from typing import Optional


class User(models.BaseUser):
    """Model for user GET and validation"""
    name: str
    telegram_alias: str
    phone: str
    address: str


class UserCreate(models.BaseUserCreate):
    """Model for user POST"""
    name: str
    telegram_alias: str
    phone: str
    address: str


class UserUpdate(models.BaseUserUpdate):
    """Model for user UPDATE"""
    name: Optional[str]
    telegram_alias: Optional[str]
    phone: Optional[str]
    address: Optional[str]


class UserDB(User, models.BaseUserDB):
    """Model for the user in Database"""
    pass
