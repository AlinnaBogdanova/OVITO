from pydantic import BaseModel, UUID4


class ProductImage(BaseModel):
    product_id: UUID4
    image_id: UUID4
