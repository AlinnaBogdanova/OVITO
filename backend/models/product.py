from pydantic import BaseModel, UUID4, Field
from uuid import uuid4
from datetime import datetime
from typing import Optional


class ProductDB(BaseModel):
    id: UUID4 = Field(default_factory=uuid4)
    title: str = Field(..., min_length=3, max_length=320)
    description: str = Field(..., min_length=3, max_length=2500)
    location: str = Field(..., min_length=3, max_length=320)
    time: datetime = Field(default_factory=datetime.now)
    category_id: UUID4 = Field(...)
    price: int = Field(..., ge=0, le=10**8)
    user_id: UUID4 = Field(...)
    status: int = Field(default=0, ge=0, le=2)


class ProductCreate(BaseModel):
    title: str = Field(..., min_length=3, max_length=320)
    description: str = Field(..., min_length=3, max_length=2500)
    location: str = Field(..., min_length=3, max_length=320)
    category_id: UUID4 = Field(...)
    price: int = Field(..., ge=0, le=10**8)


class ProductUpdate(BaseModel):
    title: Optional[str] = Field(min_length=3, max_length=320)
    description: Optional[str] = Field(min_length=3, max_length=2500)
    location: Optional[str] = Field(min_length=3, max_length=320)
    category_id: Optional[UUID4]
    price: Optional[int] = Field(ge=0, le=10 ** 8)
