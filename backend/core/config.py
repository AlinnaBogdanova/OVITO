from starlette.config import Config
from databases import DatabaseURL
from starlette.datastructures import Secret
import os

config = Config(".env")

# DB
SECRET_KEY = config("SECRET_KEY", cast=Secret, default="supersecret")
POSTGRES_USER = config("POSTGRES_USER", cast=str)
POSTGRES_PASSWORD = config("POSTGRES_PASSWORD", cast=Secret)
POSTGRES_SERVER = config("POSTGRES_SERVER", cast=str)
POSTGRES_PORT = config("POSTGRES_PORT", cast=str)
POSTGRES_DB = config("POSTGRES_DB", cast=str)


DATABASE_URL = config(
    "DATABASE_URL",
    cast=DatabaseURL,
    default=f"postgresql://{POSTGRES_USER}:{POSTGRES_PASSWORD}@{POSTGRES_SERVER}:{POSTGRES_PORT}/{POSTGRES_DB}")

# Auth
SECRET = config("SECRET", cast=Secret, default="supersecret")
LIFE_TIME = config("LIFE_TIME", cast=int, default=3600)
COOKIE_NAME = config("COOKIE_NAME", cast=str, default="csrf")
SAME_SITE = config("SAME_SITE", cast=str, default="strict")
HTTPONLY = config("HTTPONLY", cast=bool, default=False)
SECURE = config("SECURE", cast=bool, default=True)
