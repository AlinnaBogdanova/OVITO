from ..tests.test_base import BaseTest
from ..pages.LoginPage import LoginPage
from ..pages.MainPage import MainPage
from ..pages.ProfilePage import ProfilePage
from ..pages.EditCategoryPage import EditCategoryPage
from ..config.config import Config
import time


class TestOvito(BaseTest):
    def test_login(self):
        login_page = LoginPage(self.driver)
        login_page.open_page(Config.LOGIN_PAGE_URL)
        login_page.login("user@example.com", "string")
        main_page = MainPage(self.driver)
        is_main_page = main_page.is_visible(main_page.SEARCH_DIV)
        assert is_main_page
        assert main_page.get_url() == Config.MAIN_PAGE_URL

    def test_create_delete_category(self):
        main_page = MainPage(self.driver)
        main_page.open_page(Config.MAIN_PAGE_URL)
        main_page.do_click(main_page.HOME_BUTTON)

        profile_page = ProfilePage(self.driver)
        is_profile_page = profile_page.is_visible(profile_page.EDIT_CATEGORY_BUTTON)
        assert is_profile_page
        assert profile_page.get_url() == Config.PROFILE_PAGE_URL
        profile_page.do_click(profile_page.EDIT_CATEGORY_BUTTON)

        edit_page = EditCategoryPage(self.driver)
        is_edit_category_page = edit_page.is_visible(edit_page.NEW_CATEGORY_BUTTON)
        assert is_edit_category_page
        assert edit_page.get_url() == Config.EDIT_CATEGORY_PAGE_URL

        categories = edit_page.get_list_of_elements(edit_page.CATEGORY)
        first_num = len(categories)

        edit_page.do_click(edit_page.NEW_CATEGORY_BUTTON)
        is_edit_enabled = edit_page.is_visible(edit_page.SAVE_BUTTON)
        assert is_edit_enabled

        edit_page.do_send_keys(edit_page.INPUT, "new category")
        edit_page.do_click(edit_page.SAVE_BUTTON)
        time.sleep(3)

        categories = edit_page.get_list_of_elements(edit_page.CATEGORY)
        second_num = len(categories)
        assert first_num == (second_num - 1)

        categories[-1].click()
        is_delete_enabled = edit_page.is_visible(edit_page.DELETE_BUTTON)
        assert is_delete_enabled

        edit_page.do_click(edit_page.DELETE_BUTTON)
        time.sleep(3)

        categories = edit_page.get_list_of_elements(edit_page.CATEGORY)
        third_num = len(categories)

        assert first_num == third_num
