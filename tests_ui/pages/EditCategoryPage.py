from ..pages.BasePage import BasePage
from selenium.webdriver.common.by import By


class EditCategoryPage(BasePage):
    NEW_CATEGORY_BUTTON = (By.CSS_SELECTOR, '[data-test="new-btn"]')
    SAVE_BUTTON = (By.CSS_SELECTOR, '[data-test="new-val-save"]')
    INPUT = (By.CSS_SELECTOR, '[type="text"]')
    CATEGORY = (By.CSS_SELECTOR, '[class="cat-card__name"]')
    DELETE_BUTTON = (By.CSS_SELECTOR, '[data-test="delete"]')
