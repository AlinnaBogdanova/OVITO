from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from ..config.config import Config
from selenium import webdriver
from selenium.webdriver.remote.webelement import WebElement
from typing import List
"""Base class for all Pages"""
"""Contains all generic methods and functionalities of all pages"""


class BasePage:
    def __init__(self, driver: webdriver.Chrome):
        self.driver = driver

    def open_page(self, page: str):
        self.driver.get(page)

    def get_list_of_elements(self, by_locator) -> List[WebElement]:
        results: List[WebElement] = WebDriverWait(self.driver, Config.TIME_WAIT).until(
                                                  EC.visibility_of_all_elements_located(by_locator))
        return results

    def do_click(self, by_locator):
        WebDriverWait(self.driver, Config.TIME_WAIT).until(EC.visibility_of_element_located(by_locator)).click()

    def do_send_keys(self, by_locator, text):
        WebDriverWait(self.driver, Config.TIME_WAIT).until(EC.visibility_of_element_located(by_locator)).send_keys(text)

    def get_element_text(self, by_locator):
        element = WebDriverWait(self.driver, Config.TIME_WAIT).until(EC.visibility_of_element_located(by_locator))
        return element.text

    def is_visible(self, by_locator):
        element = WebDriverWait(self.driver, Config.TIME_WAIT).until(EC.visibility_of_element_located(by_locator))
        return bool(element)

    def get_title(self, title):
        WebDriverWait(self.driver, Config.TIME_WAIT).until(EC.title_is(title))
        return self.driver.title

    def do_press_keys(self, by_locator, key):
        WebDriverWait(self.driver, Config.TIME_WAIT).until(EC.visibility_of_element_located(by_locator)).send_keys(key)

    def get_title_with_text(self, text):
        WebDriverWait(self.driver, Config.TIME_WAIT).until(EC.title_contains(text))
        return self.driver.title

    def get_url(self):
        return self.driver.current_url

    def get_element_attribute(self, by_locator, attribute):
        element = WebDriverWait(self.driver, Config.TIME_WAIT).until(EC.visibility_of_element_located(by_locator))
        return element.get_attribute(attribute)

    def go_to_url(self, url):
        self.driver.get(url)