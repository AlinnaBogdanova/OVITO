from ..pages.BasePage import BasePage
from selenium.webdriver.common.by import By


class LoginPage(BasePage):
    LOGIN_INPUT = (By.CSS_SELECTOR, '[type="text"]')
    PASSWORD_INPUT = (By.CSS_SELECTOR, '[type="text"]')
    ALL_INPUTS = (By.CSS_SELECTOR, '[type="text"]')
    LOGIN_BUTTON = (By.CSS_SELECTOR, '[data-test="loginBtn"]')

    def login(self, login: str, password: str):
        inputs = self.get_list_of_elements(self.ALL_INPUTS)
        inputs[0].send_keys(login)
        inputs[1].send_keys(password)
        self.do_click(self.LOGIN_BUTTON)
