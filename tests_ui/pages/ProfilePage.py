from ..pages.BasePage import BasePage
from selenium.webdriver.common.by import By


class ProfilePage(BasePage):
    EDIT_CATEGORY_BUTTON = (By.CSS_SELECTOR, '[href="#/user/0/categories"]')
