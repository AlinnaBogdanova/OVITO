from ..pages.BasePage import BasePage
from selenium.webdriver.common.by import By


class MainPage(BasePage):
    SEARCH_DIV = (By.CSS_SELECTOR, '[class="search"]')
    HOME_BUTTON = (By.CSS_SELECTOR, '[href="#/user/0/list"]')
