class Config:
    BASE_URL = "http://ec2-18-216-132-143.us-east-2.compute.amazonaws.com"
    TIME_WAIT = 20  # Time to wait for the visibility of the element
    HOME_PAGE_TITLE = "ovito"
    LOGIN_PAGE_URL = "http://ec2-18-216-132-143.us-east-2.compute.amazonaws.com/#/login"
    MAIN_PAGE_URL = "http://ec2-18-216-132-143.us-east-2.compute.amazonaws.com/#/"
    PROFILE_PAGE_URL = "http://ec2-18-216-132-143.us-east-2.compute.amazonaws.com/#/user/0/list"
    EDIT_CATEGORY_PAGE_URL = "http://ec2-18-216-132-143.us-east-2.compute.amazonaws.com/#/user/0/categories"
