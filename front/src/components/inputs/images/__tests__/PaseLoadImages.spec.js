import { mount } from '@vue/test-utils'
import BaseLoadImages from '../BaseLoadImages.vue';

describe('BaseLoadImages.vue', () => {
  // from: https://zaengle.com/blog/mocking-file-upload-in-vue-with-jest
  it('If you upload the image, one more image is displayed', async () => {
    const wrapper = mount(BaseLoadImages)
    // mocking of file reader
    const fileReaderSpy = jest.spyOn(FileReader.prototype, 'readAsDataURL').mockImplementation(() => null)
    const file = {
      value: 'some small text',
      name: 'image.png',
      size: 500,
      type: 'image/png',
    }

    await wrapper.vm.selectFile({ target: { files: [file] } })

    expect(fileReaderSpy).toHaveBeenCalledWith(file)
    expect(wrapper.emitted('add' )[0][0]).toBe(file)
  })
})

