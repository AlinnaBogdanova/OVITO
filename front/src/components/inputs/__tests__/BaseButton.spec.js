import { mount } from '@vue/test-utils'
import BaseButton from '../BaseButton.vue';

describe('BaseButton.vue', () => {
  it('The default bg color is #7F8CFD', () => {
    const wrapper = mount(BaseButton, { slots:
      { default : 'ButtonText' }
    })
    expect(wrapper.html()).toContain('rgb(127, 140, 253)')
  })
})

describe('BaseButton.vue', () => {
  it('If you give some value, it is displayed in the slot', () => {
    const wrapper = mount(BaseButton, { slots:
      { default : 'ButtonText' }
    })
    expect(wrapper.text()).toContain('ButtonText')
  })
})

describe('BaseButton.vue', () => {
  it('The non-default color may be set to button', () => {
    const wrapper = mount(BaseButton, { props:
      { bgColor: '#123456'}
    })
    expect(wrapper.html()).not.toContain('#7F8CFD')
    expect(wrapper.html()).not.toContain('rgb(127, 140, 253)')
    // internally, the clor is changed to rgb format
    expect(wrapper.html()).toContain('rgb(18, 52, 86)')
  })
})