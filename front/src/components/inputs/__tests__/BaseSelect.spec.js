import { mount } from '@vue/test-utils'
import BaseSelect from '../BaseSelect.vue';

describe('BaseSelect.vue', () => {
  it('The option list is hidden by default', () => {
    const wrapper = mount(BaseSelect, { props: {
        options: [
          { id: 0, label: 'label1' },
          { id: 1, label: 'label2' }
        ],
        value: '-1',
      }})
    expect(wrapper.get('[data-test="options"]').isVisible()).toBe(false)
  })
})
describe('BaseSelect.vue', () => {
  it('The option list is visible after focus', async () => {
    const wrapper = mount(BaseSelect, { props: {
        options: [
          { id: 0, label: 'label1' },
          { id: 1, label: 'label2' }
        ],
        value: '-1',
      }})

    await wrapper.get('[data-test="text"]').trigger('focus')
    expect(wrapper.getCurrentComponent().data.showOptions).toBe(true)
    expect(wrapper.find('[data-test="options"]').exists()).toBe(true)
    expect(wrapper.get('[data-test="options"]').isVisible()).toBe(true)
  })
})
describe('BaseSelect.vue', () => {
  it('The option list is not visible after outside click', async () => {
    const wrapper = mount(BaseSelect, { props: {
        options: [
          { id: 0, label: 'label1' },
          { id: 1, label: 'label2' }
        ],
        value: '-1',
      }})
    wrapper.get('[data-test="text"]').trigger('focus')
    await wrapper.trigger('blur')
    expect(wrapper.get('[data-test="options"]').isVisible()).toBe(false)
  })
})
describe('BaseSelect.vue', () => {
  it('The option list is filtered on typing', async () => {
    const wrapper = mount(BaseSelect, { props: {
        options: [
          { id: 0, label: 'label1' },
          { id: 1, label: 'label2' },
          { id: 2, label: 'label11' }
        ],
        value: '-1',
      }})
    wrapper.get('[data-test="text"]').trigger('focus')
    expect(wrapper.getCurrentComponent().data.showOptions).toBe(true)
    await wrapper.get('[data-test="text"]').setValue('1')
    expect(wrapper.getCurrentComponent().data.text).toBe('1')
    expect(wrapper.getCurrentComponent().data.selectedOptions.length).toBe(2)
    expect(wrapper.getCurrentComponent().data.showOptions).toBe(true)
    expect(wrapper.findAll('.options__option').length).toBe(2)
  })
})
describe('BaseSelect.vue', () => {
  it('On option click the value is emitted and its text is displayed in the input', async () => {
    const wrapper = mount(BaseSelect, { props: {
        options: [
          { id: 0, label: 'label1' },
          { id: 1, label: 'label2' },
          { id: 2, label: 'label11' }
        ],
        value: '-1',
      }})
    await wrapper.get('[data-test="text"]').trigger('focus')

    expect(wrapper.getCurrentComponent().data.showOptions).toBe(true)
    const targetOption = wrapper.findAll('.options__option')[1]

    await targetOption.trigger('click')
    expect(wrapper.getCurrentComponent().data.text).toBe('label2')
    expect(wrapper.emitted()).toHaveProperty('select')
    expect(wrapper.emitted('select')[0][0]).toBe(1)
  })
})

// root component tests

describe('BaseSelect.vue', () => {
  it('Empty wrapper is not required', () => {
    const wrapper = mount(BaseSelect, { props: { value: '', options: [] } })
    expect(wrapper.vm.required).toBe(false)
  })
})

describe('BaseSelect.vue', () => {
  it('Empty wrapper has no title', () => {
    const wrapper = mount(BaseSelect, { props: { value: '', options: [] } })
    expect(wrapper.find('[data-test="title"]').exists()).toBe(false)
  })
})

describe('BaseSelect.vue', () => {
  it('If required and empty, the border appears', () => {
    const wrapper = mount(BaseSelect, {
      props: {
        required: true,
        value: '',
        options: [],
      }
    })
    const label = wrapper.get('[data-test="label"]')
    expect(label.element.classList).toContain('error')
  })
})

describe('BaseSelect.vue', () => {
  it('If there is no title, no title is displayed', () => {
    const wrapper = mount(BaseSelect, {
      props: {
        value: '',
        title: '',
        options: [],
      }
    })
    const wrapperText = wrapper.find('[data-test="title"]')
    expect(wrapperText.exists()).toBe(false)
  })
})

describe('BaseSelect.vue', () => {
  it('If there is a title, title is displayed', () => {
    const wrapper = mount(BaseSelect, {
      props: {
        value: '',
        title: 'someTitle',
        options: [],
      }
    })
    const title = wrapper.get('[data-test="title"]')
    expect(title.isVisible()).toBe(true)
    expect(title.text()).toBe('someTitle')
  })
})