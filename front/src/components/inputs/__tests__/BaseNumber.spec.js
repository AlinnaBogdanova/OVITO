import { mount } from '@vue/test-utils';
import BaseNumber from '../BaseNumber.vue';

describe('BaseNumber.vue', () => {
  it('If you give some value, it is displayed', async () => {
    const wrapper = mount(BaseNumber, { props: { value: 1010 } })
    expect(wrapper.getCurrentComponent().props.value).toEqual(1010)
    expect(wrapper.find('input[type="number"]').element.value).toEqual("1010")
  })
})

describe('BaseNumber.vue', () => {
  it('If you type some value, emit is happening', async () => {
    const wrapper = mount(BaseNumber, { props: { value: 1 } } )
    const input = wrapper.find('input')
    // simulate typing
    input.element.value += '123'
    await input.trigger('input')

    expect(wrapper.emitted('update:value')[0][0]).toBe(1123)
  })
})

describe('BaseNumber.vue', () => {
  it('If you type the value, which is not a number, value became empty', async () => {
    const wrapper = mount(BaseNumber, { props: { value: 1} } )
    const input = wrapper.find('input')
    // simulate typing
    input.element.value += 'something'
    await input.trigger('input')

    expect(wrapper.emitted('update:value')[0][0]).toBe(0)
  })
})

// root tests

describe('BaseNumber.vue', () => {
  it('Empty wrapper is not required', () => {
    const wrapper = mount(BaseNumber, { props: { value: 0 } })
    expect(wrapper.vm.required).toBe(false)
  })
})

describe('BaseNumber.vue', () => {
  it('Empty wrapper has no title', () => {
    const wrapper = mount(BaseNumber, { props: { value: 0 } })
    expect(wrapper.find('[data-test="title"]').exists()).toBe(false)
  })
})

describe('BaseNumber.vue', () => {
  it('If required and empty, the border appears', () => {
    const wrapper = mount(BaseNumber, {
      props: {
        required: true,
        value: 0,
      }
    })
    const label = wrapper.get('[data-test="label"]')
    expect(label.element.classList).toContain('error')
  })
})

describe('BaseNumber.vue', () => {
  it('If there is no title, no title is displayed', () => {
    const wrapper = mount(BaseNumber, {
      props: {
        title: '',
        value: 0,
      }
    })
    const wrapperText = wrapper.find('[data-test="title"]')
    expect(wrapperText.exists()).toBe(false)
  })
})

describe('BaseNumber.vue', () => {
  it('If there is title, title is displayed', () => {
    const titleText = 'someTitle'
    const wrapper = mount(BaseNumber, {
      props: {
        title: titleText,
        value: 0,
      }
    })
    const title = wrapper.get('[data-test="title"]')
    expect(title.isVisible()).toBe(true)
    expect(title.text()).toBe(titleText)
  })
})
