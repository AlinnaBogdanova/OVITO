import { mount } from '@vue/test-utils';
import BaseInput from '../BaseInput.vue';

describe('BaseInput.vue', () => {
  it('If you give some value, it is displayed', async () => {
    const wrapper = mount(BaseInput, { props: { value: ''} })
    expect(wrapper.getCurrentComponent().props.value).not.toBe('some')
    await wrapper.setProps({ value: 'some' })
    expect(wrapper.getCurrentComponent().props.value).toBe('some')
    expect(wrapper.find('input').element.value).toBe('some')
  })
})

describe('BaseInput.vue', () => {
  it('If you type some value, emit is happening', async () => {
    const wrapper = mount(BaseInput, { props: { value: 'some'} } )
    const input = wrapper.find('input')
    // simulate typing
    input.element.value += 'Value'
    await input.trigger('input')

    expect(wrapper.emitted('update:value')[0][0]).toBe('someValue')
  })
})

// tests for root component

describe('BaseInput.vue', () => {
  it('Empty wrapper is not required', () => {
    const wrapper = mount(BaseInput, { props: { value: '' } })
    expect(wrapper.vm.required).toBe(false)
  })
})

describe('BaseInput.vue', () => {
  it('Empty wrapper has no title', () => {
    const wrapper = mount(BaseInput, { props: { value: '' } })
    expect(wrapper.find('[data-test="title"]').exists()).toBe(false)
  })
})

describe('BaseInput.vue', () => {
  it('If required and empty, the border appears', () => {
    const wrapper = mount(BaseInput, {
      props: {
        required: true,
        value: '',
      }
    })
    const label = wrapper.get('[data-test="label"]')
    expect(label.element.classList).toContain('error')
  })
})

describe('BaseInput.vue', () => {
  it('If there is no title, no title is displayed', () => {
    const wrapper = mount(BaseInput, {
      props: {
        title: '',
        value: '',
      }
    })
    const wrapperText = wrapper.find('[data-test="title"]')
    expect(wrapperText.exists()).toBe(false)
  })
})

describe('BaseInput.vue', () => {
  it('If there is title, title is displayed', () => {
    const titleText = 'someTitle'
    const wrapper = mount(BaseInput, {
      props: {
        title: titleText,
        value: '',
      }
    })
    const title = wrapper.get('[data-test="title"]')
    expect(title.isVisible()).toBe(true)
    expect(title.text()).toBe(titleText)
  })
})
