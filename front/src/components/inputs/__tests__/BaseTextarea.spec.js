import { mount } from '@vue/test-utils';
import BaseTextarea from '../BaseTextarea.vue';

describe('BaseTextarea.vue', () => {
  it('If you give some value, it is displayed', async () => {
    const wrapper = mount(BaseTextarea, { props: { value: ''} })
    expect(wrapper.getCurrentComponent().props.value).not.toBe('some')
    await wrapper.setProps({ value: 'some' })
    expect(wrapper.getCurrentComponent().props.value).toBe('some')
    expect(wrapper.find('textarea').element.value).toBe('some')
  })
})

describe('BaseTextarea.vue', () => {
  it('If you type some value, emit is happening', async () => {
    const wrapper = mount(BaseTextarea, { props: { value: 'some'} } )
    const input = wrapper.find('textarea')
    // simulate typing
    input.element.value += 'Value'
    await input.trigger('input')

    expect(wrapper.emitted('update:value')[0][0]).toBe('someValue')
  })
})

// tests related to root component

describe('BaseTextarea.vue', () => {
  it('Empty wrapper is not required', () => {
    const wrapper = mount(BaseTextarea)
    expect(wrapper.vm.required).toBe(false)
  })
})

describe('BaseTextarea.vue', () => {
  it('Empty wrapper has no title', () => {
    const wrapper = mount(BaseTextarea)
    expect(wrapper.find('[data-test="title"]').exists()).toBe(false)
  })
})

describe('BaseTextarea.vue', () => {
  it('Empty wrapper has no defined value', () => {
    const wrapper = mount(BaseTextarea)
    expect(wrapper.vm.value).toBe(undefined)
  })
})

describe('BaseTextarea.vue', () => {
  it('If required and empty, the border appears', () => {
    const wrapper = mount(BaseTextarea, {
      props: {
        required: true,
        value: undefined,
      }
    })
    const label = wrapper.get('[data-test="label"]')
    expect(label.element.classList).toContain('error')
  })
})

describe('BaseTextarea.vue', () => {
  it('If there is no title, no title is displayed', () => {
    const wrapper = mount(BaseTextarea, {
      props: {
        title: ''
      }
    })
    const wrapperText = wrapper.find('[data-test="title"]')
    expect(wrapperText.exists()).toBe(false)
  })
})

describe('BaseTextarea.vue', () => {
  it('If there is title, title is displayed', () => {
    const titleText = 'someTitle'
    const wrapper = mount(BaseTextarea, {
      props: {
        title: titleText
      }
    })
    const title = wrapper.get('[data-test="title"]')
    expect(title.isVisible()).toBe(true)
    expect(title.text()).toBe(titleText)
  })
})