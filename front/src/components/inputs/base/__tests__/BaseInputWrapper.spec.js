import { mount } from '@vue/test-utils'
import BaseInputWrapper from '../BaseInputWrapper.vue'

describe('BaseInputWrapper.vue', () => {
  it('Empty wrapper is not required', () => {
    const wrapper = mount(BaseInputWrapper)
    expect(wrapper.vm.required).toBe(false)
  })
})

describe('BaseInputWrapper.vue', () => {
  it('Empty wrapper has no title', () => {
    const wrapper = mount(BaseInputWrapper)
    expect(wrapper.find('[data-test="title"]').exists()).toBe(false)
  })
})

describe('BaseInputWrapper.vue', () => {
  it('Empty wrapper has no defined value', () => {
    const wrapper = mount(BaseInputWrapper)
    expect(wrapper.vm.value).toBe(undefined)
  })
})

describe('BaseInputWrapper.vue', () => {
  it('If required and empty, the border appears', () => {
    const wrapper = mount(BaseInputWrapper, {
      props: {
        required: true,
        value: undefined,
      }
    })
    const label = wrapper.get('[data-test="label"]')
    expect(label.element.classList).toContain('error')
  })
})

describe('BaseInputWrapper.vue', () => {
  it('If there is no title, no title is displayed', () => {
    const wrapper = mount(BaseInputWrapper, {
      props: {
        title: ''
      }
    })
    const wrapperText = wrapper.find('[data-test="title"]')
    expect(wrapperText.exists()).toBe(false)
  })
})

describe('BaseInputWrapper.vue', () => {
  it('If there is title, title is displayed', () => {
    const titleText = 'someTitle'
    const wrapper = mount(BaseInputWrapper, {
      props: {
        title: titleText
      }
    })
    const title = wrapper.get('[data-test="title"]')
    expect(title.isVisible()).toBe(true)
    expect(title.text()).toBe(titleText)
  })
})