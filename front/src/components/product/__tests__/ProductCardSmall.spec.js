import { mount } from '@vue/test-utils'
import ProductCardSmall from '../ProductCardSmall.vue';

describe('ProductCardSmall.vue', () => {
  it('If there are images, they are displayed', () => {
    const wrapper = mount(ProductCardSmall, {
      props: { product: { imgs: [{}]}},
    })
    expect(wrapper.find('img').exists()).toBe(true)
  })
})