import { mount } from '@vue/test-utils'
import ProductCardBig from '../ProductCardBig.vue';

describe('ProductCardBig.vue', () => {
  it('If there are images, they are displayed', () => {
    const wrapper = mount(ProductCardBig, {
      props: { product: { imgs: [{}]}},
    })
    expect(wrapper.find('img').exists()).toBe(true)
  })
})