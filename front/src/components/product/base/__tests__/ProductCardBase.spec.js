import { mount } from '@vue/test-utils'
import ProductCardBase from '../ProductCardBase.vue';

describe('ProductCardBase.vue', () => {
  it('If you give some value, it is displayed in the slot', () => {
    const wrapper = mount(ProductCardBase, {
      slots: { default : 'Some Random Text' },
      props: { product: {}},
    })
    expect(wrapper.text()).toContain('Some Random Text')
  })
})
describe('ProductCardBase.vue', () => {
  it('If you give product without title, there are no h-related tags', () => {
    const wrapper = mount(ProductCardBase, {
      slots: { default : 'Some Random Text' },
      props: { product: {}},
    })

    for (let depth = 1; depth < 7; depth++)
      expect(wrapper.find(`h${depth}`).exists()).toBe(false)
  })
})
describe('ProductCardBase.vue', () => {
  it('If you give some product with title, it is displayed', () => {
    const wrapper = mount(ProductCardBase, { props:
      { product: { title: 'SomeTTitle' } }
    })
    expect(wrapper.html()).toContain('SomeTTitle')
  })
})