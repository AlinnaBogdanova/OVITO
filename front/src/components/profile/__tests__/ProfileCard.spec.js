import { mount } from '@vue/test-utils'
import ProfileCard from '../ProfileCard.vue';

describe('ProfileCard.vue', () => {
  it('If there is a username, it is displayed', () => {
    const wrapper = mount(ProfileCard, {
      props: { user: { username: 'useRname' }},
    })
    expect(wrapper.text()).toContain('useRname')
  })
})

describe('ProfileCard.vue', () => {
  it('If there is an address, it is displayed', () => {
    const wrapper = mount(ProfileCard, {
      props: { user: { address: 'aDDress' }},
    })
    expect(wrapper.text()).toContain('aDDress')
  })
})