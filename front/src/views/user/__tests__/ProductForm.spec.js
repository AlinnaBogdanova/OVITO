import { mount, shallowMount, flushPromises } from '@vue/test-utils'
import ProductForm from '../ProductForm.vue'
import catApi from '@/api/cats'
import { createRouter, createWebHashHistory } from 'vue-router'
import { nextTick } from 'vue'
import productApi from '@/api/products'
import routes from "@/router/routes"

const mockListCats = [
  { id: 0, name: 'Cat 1' },
  { id: 1, name: 'Cat 2' },
  { id: 2, name: 'Cat 3' }
]
jest.spyOn(catApi, 'list').mockResolvedValue(mockListCats)
jest.spyOn(productApi, 'create').mockImplementation(v => v)
jest.spyOn(productApi, 'update').mockImplementation(v => v)
jest.spyOn(productApi, 'getProduct').mockImplementation(async id => ({
  category_id: "fab49532-95f1-453e-82ca-73044c27351e",
  description: "оичлоуичловци",
  id: id,
  location: "8t757rf vhgv",
  price: 10001,
  status: 0,
  time: "2022-05-09T13:23:51.943859",
  title: "мормомо",
  user_id: "24ba6d75-aa80-4d9f-8d4b-e14af3ad0b41",
}))

let router;
beforeEach(async () => {
  router = createRouter({
    history: createWebHashHistory(),
    routes: routes,
  })
});

describe('ProductForm.vue', () => {
  it('If id is given, the request id send to the backend', async () => {
    router.push('/user/1/edit/12')
    await router.isReady()
    const wrapper = shallowMount(ProductForm, {
      global: {
        plugins: [router]
      }
    })
    await flushPromises()
    await nextTick()
    expect(catApi.list).toHaveBeenCalledTimes(1)
    expect(wrapper.vm.productId).not.toBe(-1)
    expect(wrapper.vm.productId).toBe(12)
    expect(productApi.getProduct).toHaveBeenCalledTimes(1)
    expect(wrapper.vm.price).toBe(10001)
  })
})

describe('ProductForm.vue', () => {
  it('If the form is new - all fields are empty', async () => {
    router.push('/user/1/create')
    await router.isReady()
    const wrapper = mount(ProductForm, {
      global: {
        plugins: [router]
      }
    })
    await flushPromises()
    await nextTick()
    const inputs = wrapper.findAll('input')
    inputs.forEach(input => {
      if (input.element.type === 'number')
        expect(input.element.value).toBe('1000')
      else if (input.element.type === 'file')
        return
      else
        expect(input.element.value).toBe('')
    })
  })
})
