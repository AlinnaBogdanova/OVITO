import { mount, flushPromises } from '@vue/test-utils'
import EditCats from '../EditCats.vue'
import catApi from '@/api/cats'
import { nextTick } from 'vue'

const mockEditCats = [
  { id: 0, name: 'name1' },
  { id: 1, name: 'name2' },
  { id: 2, name: 'name3' }
]
jest.spyOn(catApi, 'list').mockResolvedValue(mockEditCats)
jest.spyOn(catApi, 'delete').mockResolvedValue({ data: null })
jest.spyOn(catApi, 'update').mockImplementation((x) => x)
jest.spyOn(catApi, 'create').mockImplementation((x) => ({ ...x, id: 100 }))


describe('EditCats.vue', () => {
  it('The number of displayed cards is equal to the number of cards in list', async () => {
    const wrapper = mount(EditCats)
    await flushPromises()
    await nextTick()
    expect(catApi.list).toHaveBeenCalledTimes(1)
    expect(wrapper.findAll('li.cat-card')).toHaveLength(3)
  })
})
describe('EditCats.vue', () => {
  it('If cat name is clicked, then it switches to a form', async () => {
    const wrapper = mount(EditCats)
    await flushPromises()
    await nextTick()
    await wrapper.findAll('li.cat-card')[1].find('span').trigger('click')
    expect(wrapper.findAll('li.cat-card')[1].html()).toContain('Save')
    expect(wrapper.findAll('li.cat-card')[1].html()).toContain('Delete')
  })
})
describe('EditCats.vue', () => {
  it('The delete event is happening, when the button is clicked', async () => {
    const wrapper = mount(EditCats)
    await flushPromises()
    await nextTick()
    // made second item active
    await wrapper.findAll('li.cat-card')[1].find('span').trigger('click')
    // trigger delete
    await wrapper.findAll('li.cat-card')[1].find('[data-test="delete"]').trigger('click')
    await flushPromises()
    await nextTick()
    // check, that the card is removed
    expect(wrapper.findAll('li.cat-card')).toHaveLength(2)
    expect(wrapper.text()).toContain('name1')
    expect(wrapper.text()).toContain('name3')
    expect(wrapper.text()).not.toContain('name2')
  })
})
describe('EditCats.vue', () => {
  it('The edit event is happening, when the button is clicked', async () => {
    const wrapper = mount(EditCats)
    await flushPromises()
    await nextTick()
    // made second item active
    await wrapper.findAll('li.cat-card')[1].find('span').trigger('click')
    // update value
    await wrapper.findAll('li.cat-card')[1].find('input').setValue('new name 2')
    // trigger edit
    await wrapper.findAll('li.cat-card')[1].find('[data-test="save"]').trigger('click')
    await flushPromises()
    await nextTick()
    // check, that the card is removed
    expect(wrapper.findAll('li.cat-card')).toHaveLength(3)
    expect(wrapper.findAll('li.cat-card')[1].text()).toContain('new name 2')
  })
})
describe('EditCats.vue', () => {
  it('New card may be added', async () => {
    const wrapper = mount(EditCats)
    await flushPromises()
    await nextTick()
    // click "New Category" button
    await wrapper.find('[data-test="new-btn"]').trigger('click')
    // insert new value
    await wrapper.find('input').setValue('NEW')
    // save
    await wrapper.find('[data-test="new-val-save"]').trigger('click')
    expect(wrapper.findAll('li.cat-card')).toHaveLength(4)
  })
})