import { mount, flushPromises } from '@vue/test-utils'
import ListProducts from '../ListProducts.vue'
import { nextTick } from 'vue'
import productApi from '@/api/products'
import { createStore } from 'vuex'

jest.spyOn(productApi, 'listOf').mockImplementation(async id => ([{
    category_id: "fab49532-95f1-453e-82ca-73044c27351e",
    description: "Some description",
    id: 2123,
    location: "Location 1",
    price: 1001,
    status: 0,
    time: "2022-05-03T13:23:51.943859",
    title: "First",
    user_id: 12,
  },
    {
    category_id: "fab49532-95f1-453e-82ca-73044c27351e",
    description: "Some other description",
    id: 1,
    location: "Location 2",
    price: 500,
    status: 0,
    time: "2022-06-09T13:23:51.943859",
    title: "Second",
    user_id: 12,
  }
]))

let $store;
let wrapper;
beforeEach(async () => {
  $store = createStore({
    state: {
      userId: 10,
    }
  })
  wrapper = mount(ListProducts, { global: { mocks: {
    $store
  }}})
  await flushPromises()
  await nextTick()
})

describe('ListProducts.vue', () => {
  it('The call to the list of users product is initiated', () => {
    expect(productApi.listOf).toHaveBeenCalledTimes(1)
    expect(productApi.listOf).toHaveBeenCalledWith(10)
  })
})

describe('ListProducts.vue', () => {
  it('The number of displayed cards is equal to the number of call outputs', () => {
    expect(wrapper.findAll('article.product-card')).toHaveLength(2)
  })
})