import { mount, flushPromises } from '@vue/test-utils'
import SearchPage from '../SearchPage.vue'
import catApi from '@/api/cats'
import { nextTick } from 'vue'
import productApi from '@/api/products'

const mockListCats = [
  { id: '0', name: 'Cat 1' },
  { id: '1', name: 'Cat 2' },
  { id: '2', name: 'Cat 3' }
]
jest.spyOn(catApi, 'list').mockResolvedValue(mockListCats)
jest.spyOn(productApi, 'list').mockImplementation(async id => ([
  {
    category_id: '0',
    description: "Some description",
    id: 2123,
    location: "Location 1",
    price: 1001,
    status: 0,
    time: "2022-05-03T13:23:51.943859",
    title: "First (aa)",
    user_id: 12,
  },
  {
    category_id: '0',
    description: "Some other description",
    id: 1,
    location: "Location 2",
    price: 500,
    status: 0,
    time: "2022-06-09T13:23:51.943859",
    title: "Second",
    user_id: 12,
  },
  {
    category_id: '1',
    description: "Some other description",
    id: 2,
    location: "Location 3",
    price: 1500,
    status: 0,
    time: "2022-06-09T13:23:51.943859",
    title: "Third (aa)",
    user_id: 14,
  }
]))

let wrapper;
beforeEach(async () => {
  wrapper = mount(SearchPage)
  await flushPromises()
  await nextTick()
})

describe('SearchPage.vue', () => {
  it('If the filters are empty, then all products are displayed', async () => {
    expect(wrapper.findAll('.product-card')).toHaveLength(3)
  })
})

describe('SearchPage.vue', () => {
  it('Initially, all inputs are empty', async () => {
    const inputs = wrapper.findAll('input')
    inputs.forEach(input => {
      expect(input.element.value).toBe('')
    })
  })
})

describe('SearchPage.vue', () => {
  it('Some filters are set, but not applied, then the data remain the same', async () => {
    await wrapper.get('[data-test="category"] [data-test="text"]').trigger('focus')
    await wrapper.findAll('.options__option')[0].trigger('click')
    expect(wrapper.vm.filter.cat).toBe('0')

    await wrapper.find('[data-test="query"] input').setValue('aa')
    expect(wrapper.findAll('.product-card')).toHaveLength(3)
  })
})

describe('SearchPage.vue', () => {
  it('If the category filter is set and applied, then data is filtered', async () => {
    await wrapper.get('[data-test="category"] [data-test="text"]').trigger('focus')
    await wrapper.findAll('.options__option')[0].trigger('click')
    expect(wrapper.vm.filter.cat).toBe('0')

    await wrapper.find('[data-test="search"]').trigger('click')
    expect(wrapper.findAll('.product-card')).toHaveLength(2)
  })
})

describe('SearchPage.vue', () => {
  it('If the query filter is set and applied, then data is filtered', async () => {
    await wrapper.find('[data-test="query"] input').setValue('aa')
    await wrapper.find('[data-test="search"]').trigger('click')
    expect(wrapper.findAll('.product-card')).toHaveLength(2)
  })
})

describe('SearchPage.vue', () => {
  it('If both filters are set and applied, then data is filtered', async () => {
    await wrapper.get('[data-test="category"] [data-test="text"]').trigger('focus')
    await wrapper.findAll('.options__option')[0].trigger('click')
    expect(wrapper.vm.filter.cat).toBe('0')

    await wrapper.find('[data-test="query"] input').setValue('aa')
    await wrapper.find('[data-test="search"]').trigger('click')
    expect(wrapper.findAll('.product-card')).toHaveLength(1)
  })
})
