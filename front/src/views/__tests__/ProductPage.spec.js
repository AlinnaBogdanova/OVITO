import { mount, flushPromises } from '@vue/test-utils'
import ProductPage from '../ProductPage.vue'
import { createRouter, createWebHashHistory } from 'vue-router'
import { nextTick } from 'vue'
import productApi from '@/api/products'
import routes from "@/router/routes"

jest.spyOn(productApi, 'getProduct').mockImplementation(async id => (
  {
    category_id: '0',
    description: "Some description",
    id: 2123,
    location: "Location 1",
    price: 1001,
    status: 0,
    time: "2022-05-03T13:23:51.943859",
    title: "First (aa)",
    user_id: 12,
  }))

let wrapper;
let router;
beforeEach(async () => {
  router = createRouter({
    history: createWebHashHistory(),
    routes: routes,
  })
  router.push('/product/121')
  await router.isReady()
  wrapper = mount(ProductPage, {
    global: {
      plugins: [router]
    }
  })
  await flushPromises()
  await nextTick()
});

describe('ProductPage.vue', () => {
  it('If id is given, the request id send to the backend', async () => {
    expect(productApi.getProduct).toHaveBeenCalledTimes(1)
    expect(productApi.getProduct).toHaveBeenCalledWith('121')
    expect(wrapper.find('.product-card').exists()).toBe(true)
  })
})
