import { mount } from '@vue/test-utils'
import RegisterForm from '../RegisterForm.vue'
import userApi from '@/api/user'
import { createStore } from 'vuex'

jest.spyOn(userApi, 'register').mockImplementation(async (data) => data.name ?
  { ...data }
  : {
    error: true,
    "detail": [
      {"loc":["body","email"],"msg":"value is not a valid email address","type":"value_error.email"},
      {"loc":["body","email"],"msg":"value is empty","type":"value_error.email"}
    ]
  }
)

describe('RegisterForm.vue', () => {
  it('Initially, all form fields are empty', async () => {
    const wrapper = mount(RegisterForm)
    const inputs = wrapper.findAll('input')
    inputs.forEach(input => {
      expect(input.element.value).toBe('')
    })
  })
})

describe('RegisterForm.vue', () => {
  it('Initially, there is no error', async () => {
    const wrapper = mount(RegisterForm)
    expect(wrapper.find('[data-test="error"]').exists()).toBe(false)
  })
})

describe('RegisterForm.vue', () => {
  it('When the button is clicked, the "register" method is called with appropriate body', async () => {

    const wrapper = mount(RegisterForm)
    await wrapper.find('[data-test="email"] input').setValue('email@email')
    await wrapper.find('[data-test="password"] input').setValue('paSSword')
    await wrapper.find('[data-test="name"] input').setValue('Some Name')
    await wrapper.find('[data-test="telegram alias"] input').setValue('@Alias')
    await wrapper.find('[data-test="phone"] input').setValue('+75671329531')
    await wrapper.find('[data-test="address"] input').setValue('Some address')


    await wrapper.find('button').trigger('click')
    expect(userApi.register).toBeCalledWith({
      email: 'email@email',
      password: 'paSSword',
      name: 'Some Name',
      telegram_alias: '@Alias',
      phone: '+75671329531',
      address: 'Some address',
    })
  })
})

describe('RegisterForm.vue', () => {
  it('If the error is received from the backend, then it is displayed and login is not called', async () => {
    const $store = createStore()
    $store.dispatch = jest.fn()

    const wrapper = mount(RegisterForm, { global: {
      mocks: {
        $store
      },
    }})

    await wrapper.find('button').trigger('click')
    const errorMsg = wrapper.find('[data-test="error"]')
    expect(errorMsg.exists()).toBe(true)
    expect(errorMsg.text()).toContain('value is not a valid email address')
    expect(errorMsg.text()).toContain('; value is empty')
    expect($store.dispatch).not.toBeCalled()
  })
})

describe('RegisterForm.vue', () => {
  it('If the form is validly filled, then the login from store is called', async () => {
    const $store = createStore()
    $store.dispatch = jest.fn()

    const wrapper = mount(RegisterForm, { global: {
      mocks: {
        $store
      },
    }})
    await wrapper.find('[data-test="email"] input').setValue('email@email')
    await wrapper.find('[data-test="password"] input').setValue('paSSword')
    await wrapper.find('[data-test="name"] input').setValue('Some Name')
    await wrapper.find('[data-test="telegram alias"] input').setValue('@Alias')
    await wrapper.find('[data-test="phone"] input').setValue('+75671329531')
    await wrapper.find('[data-test="address"] input').setValue('Some address')

    await wrapper.find('button').trigger('click')

    const errorMsg = wrapper.find('[data-test="error"]')
    expect(errorMsg.exists()).toBe(false)

    expect($store.dispatch).toBeCalledWith('userLogin', {
      login: 'email@email',
      password: 'paSSword',
    })
  })
})