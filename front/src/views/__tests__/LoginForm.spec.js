import { mount } from '@vue/test-utils'
import LoginForm from '../LoginForm.vue'
import { createStore } from 'vuex'


describe('LoginForm.vue', () => {
  it('Initially, all form fields are empty', async () => {
    const wrapper = mount(LoginForm)
    const inputs = wrapper.findAll('input')
    inputs.forEach(input => {
      expect(input.element.value).toBe('')
    })
  })
})

describe('LoginForm.vue', () => {
  it('When the button is clicked, the "login" method is called with appropriate body', async () => {

    const $store = createStore()
    $store.dispatch = jest.fn()

    const wrapper = mount(LoginForm, { global: {
      mocks: {
        $store
      },
    }})
    await wrapper.find('[data-test="login"] input').setValue('email@email')
    await wrapper.find('[data-test="password"] input').setValue('paSSword')

    await wrapper.find('[data-test="loginBtn"]').trigger('click')

    expect($store.dispatch).toBeCalledWith('userLogin', {
      login: 'email@email',
      password: 'paSSword',
    })
  })
})
