import { createApp } from 'vue';
import router from './router';
import store from './store';
import App from './App.vue';
import axios from 'axios';

axios.defaults.withCredentials = true;


createApp(App)
  .use(router)
  .use(store)
  .mount('#app');
