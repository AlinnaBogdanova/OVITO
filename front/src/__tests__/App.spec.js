import { mount, flushPromises } from '@vue/test-utils'
import App from '../App.vue'
import { createRouter, createWebHashHistory } from 'vue-router'
import { nextTick } from 'vue'
import routes from "@/router/routes"
import { createStore } from 'vuex'


let router;
let wrapper;
let $store;
beforeEach(async () => {
  router = createRouter({
    history: createWebHashHistory(),
    routes: routes,
  })
  $store = createStore({
    state: {
      userId: 10,
    }
  })
  router.push('/')
  await router.isReady()
  wrapper = mount(App, {
    global: {
      plugins: [router],
      mocks: { $store },
    },
  })
  await flushPromises()
  await nextTick()
});

describe('App.vue', () => {
  it('Location is Kazan', async () => {
    expect(wrapper.find('nav').text()).toContain('Kazan')
  })
})
