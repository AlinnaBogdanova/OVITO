import { api } from './index';

let userApi = {
  async register(form) {
    return api
      .post('/api/v1/account/register', form)
      .catch(({ detail }) => ({...detail, error: true}))
  },

  async login(user) {

    return api.post('/api/v1/account/login', user, {
        withCredentials: true,
        credentials: 'include',
      });
  },

  async logout() {
    return api.post('/api/v1/account/logout');
  },

  async me() {
    return api.get('/api/v1/account/me');
  }

};

export default userApi;