import axios from 'axios';

const api = axios.create({
  baseURL: 'http://ec2-18-216-132-143.us-east-2.compute.amazonaws.com',
  withCredentials: true,
  xsrfCookieName: 'csrf',
  headers: {
   'Access-Control-Allow-Origin': 'http://ec2-18-216-132-143.us-east-2.compute.amazonaws.com',
  },
});

const COOKIE_EXPIRED_MSG = 'Token has expired'
api.interceptors.response.use((response) => {
  return response
}, async (error) => {
  const error_message = error.response.data.msg
  switch (error.response.status) {
    case 401:
      if (!error.config.retry && error_message === COOKIE_EXPIRED_MSG) {
        error.config.retry = true
        api.defaults.xsrfCookieName = 'csrf';
        await api.post('/refresh_token')
        api.defaults.xsrfCookieName = 'csrf';
        return api(error.config)
      }
      break;
    case 404:
      console.log('404')
      // router.push('/404');
      break;
    default:
      break;
  }
  return error.response;
});
export { api };
