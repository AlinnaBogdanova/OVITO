import { api } from './index';

let productApi = {
  async list() {
    return api
      .get('/api/v1/product/')
      .then(({ data }) => Promise.all(data.map(async p => {
        const imgs = await this.getImages(p.id)
        return {...p, imgs}
      })));
  },

    async listOf(uid) {
      return api
        .get(`/api/v1/product/of/?user_id=${uid}`)
        .then(({ data }) => Promise.all(data.map(async p => {
          const imgs = await this.getImages(p.id)
          return {...p, imgs}
        })));
  },

  async getProduct(product_id) {
    const imgs = await this.getImages(product_id)
    return await api.get(`/api/v1/product/${product_id}`)
      .then(({ data }) => ({...data, imgs}));
  },

  async create(product) {
    return api.post('/api/v1/product/', product)
      .then(({ data }) => data);
  },

  async update(product) {
    return api.patch(`/api/v1/product/${product.id}`, product)
      .then(({ data }) => data);
  },

  async addImage(product, image) {
    return api.post('/api/v1/product/add_image', { image_id: image, product_id: product});
  },

  async getImages(productId) {
    return api.get(`/api/v1/product/${productId}/images`)
      .then(({ data }) => data);
  },

  async uploadImage(image) {
    return api.post('/api/v1/image/', image)
      .then(({ data }) => data)
  },



};

export default productApi;