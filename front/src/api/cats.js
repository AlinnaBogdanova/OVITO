import { api } from './index';

let catApi = {
  async list() {
    return api.get('/api/v1/category/')
     .then(({ data }) => data);
  },

  async create(cat) {
    return api.post('/api/v1/category/', cat)
      .then(({ data }) => data);
  },

  async update(cat) {
    return api.patch(`/api/v1/category/${cat.id}`, cat)
      .then(({ data }) => data);
  },

  async delete(cat) {
    return api.delete(`/api/v1/category/${cat.id}`)
      .then(({ data }) => data);
  },

};

export default catApi;