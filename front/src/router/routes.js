export default [
  {
    path: '/',
    name: 'search',
    component: () => import('../views/SearchPage'),
    meta: { requiresAuth: true },
  },
  {
    path: '/login',
    name: 'login',
    component: () => import('../views/LoginForm')
  },
  {
    path: '/register',
    name: 'register',
    component: () => import('../views/RegisterForm')
  },
  {
    path: '/user/:id',
    name: 'user',
    component:  () => import('../views/UserPage'),
    meta: { requiresAuth: true },
    children: [
      {
        path: 'list',
        name: 'user-edit',
        component: () => import('../views/user/ListProducts')
      },
      {
        path: 'create',
        name: 'user-create',
        component: () => import('../views/user/ProductForm'),
      },
      {
        path: 'edit/:productId',
        name: 'user-product',
        component: () => import('../views/user/ProductForm'),
      },
      {
        path: 'categories',
        name: 'user-cats',
        component: () => import('../views/user/EditCats')
      }

    ]
  },
  { path: '/product/:id', name: 'product', component:  () => import('../views/ProductPage') },
]
