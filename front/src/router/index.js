import { createRouter, createWebHashHistory } from 'vue-router';
import routes from '@/router/routes'

let router = createRouter({
  history: createWebHashHistory(),
  routes: routes
});

// from https://www.smashingmagazine.com/2020/10/authentication-in-vue-js/
router.beforeEach((to, from, next) => {
  if(to.matched.some(record => record.meta.requiresAuth)) {
    if (document.cookie.includes('csrf')) {
      next();
      return;
    }
    next('/login');
  } else {
    next();
  }
});

export default router;