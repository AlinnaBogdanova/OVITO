import { createStore } from 'vuex';
import userApi from '@/api/user'

let store = createStore(
  {
  state () {
    return {
      userId: 0,
      error: '',
      user: undefined,
    }
  },
  getters: {
    isAuthenticated(state) {
      return !!state.user
    }
  },
  mutations: {
    setUser(state, user) {
      state.user = user;
      state.userId = user.id;
    },
    setError({ state }, error) {
      state.error = error;
    }
  },
  actions: {
    async userLogin({ commit }, { login, password }) {
      let form = new FormData();
      form.append('username', login);
      form.append('password', password);
      await userApi.login(form);

      const { data } = await userApi.me();
      commit('setUser', data);
    }
  },

});

export default store;
